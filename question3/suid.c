#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    int ruid = getuid();
    int euid = geteuid();
    int rgid = getgid();
    int egid = getegid();

    FILE* f;

    ssize_t r;
    size_t len = 0;
    char* line = NULL;

    printf("RUID: %d, EUID: %d, RGID: %d, EGID: %d\n", ruid, euid, rgid, egid);

    if (argc < 2) {
        printf("Missing argument\n");
        exit(EXIT_FAILURE);
    }

    f = fopen(argv[1], "r");

    if (f == NULL) {
        perror("Cannot open file\n");
        exit(EXIT_FAILURE);
    }

    while((r = getline(&line, &len, f)) != -1) printf("%s", line);

    printf("\n");

    fclose(f);

    if (line != NULL) free(line);

    exit(EXIT_SUCCESS);
}
