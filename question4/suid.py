#!/usr/bin/env python3
import os

if __name__ == "__main__":
    egid = os.getegid()
    euid = os.geteuid()

    print(f"""EGID: {egid}, EUID: {euid}""")

