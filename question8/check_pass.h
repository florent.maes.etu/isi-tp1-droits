#ifndef __CHECK_PASS_H__
#define __CHECK_PASS_H__


char *read_pass(char *username);
int check_pass(char *password);

#endif