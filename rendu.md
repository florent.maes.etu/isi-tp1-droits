# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Maës Florent, email: florent.maes.etu@univ-lille.fr

- Masquelier Dylan, email: dylan.masquelier.etu@univ-lille.fr

## Question 1

L'utilisateur `toto` n'a pas le droit d'écrire dans le fichier. En effet, il a beau être dans le groupe `ubuntu` qui a le droit d'écrire dans le fichier, il est explicitement spécifié que le propriétaire `toto` n'a pas le droit d'écrire dans ce fichier.

 Comme c'est `toto` qui a lancé le processus, le processus a le même `EUID` que `toto` et c'est le premier triplet qui est pris en compte (comme `toto` est le propriétaire du fichier), donc `toto` ne peut pas écrire dans le fichier.

```shell
# crée l'utilisateur toto
ubuntu:~$ sudo adduser toto
# ajoute toto au groupe ubuntu
ubuntu:~$ sudo usermod -a -G ubuntu toto

# crée le fichier myfile.txt
ubuntu:~$ touch myfile.txt
# change les droits de myfile.txt en -r--rw-r-- ubuntu ubuntu ...
ubuntu:~$ sudo chmod 464 myfile.txt
# fait de toto le propriétaire du fichier
ubuntu:~$ sudo chown toto myfile.txt

# changement vers l'utilisateur toto
ubuntu:~$ su toto

# essai d'écriture dans myfile.txt avec echo
toto:~$ echo "123" > myfile.txt
bash: myfile.txt: Permission denied
```

## Question 2

Le caractère 'x' pour un répertoire indique si le répertoire est accessible (par un utilisateur ou groupe).

Ici, nous avons spécifié que le groupe `ubuntu` ne peut pas accéder au dossier `mydir`. Ainsi, avec l'utilisateur `toto`, il est impossible d'accéder à ce dossier.

Il se produit l'affichage suivant:
```shell
-????????? ? ?       ?          ?            ? data.txt
```

L'utilisateur `toto` n'arrive à voir que le nom du fichier `data.txt`. En effet, le dossier n'étant pas "exécutable" par `toto`, il ne peut pas non plus accéder à son contenu.

```shell
ubuntu:~$ mkdir mydir
# change les droits de mydir en drwxrw-rwx
ubuntu:~$ sudo chown 767 mydir
ubuntu:~$ touch mydir/data.txt

ubuntu:~$ su toto

toto:~$ cd mydir
bash: cd: mydir: Permission denied

toto:~$ ls -al mydir
drwxrw-rwx 1 ubuntu ubuntu 4096 Jan 12 15:26 .
drwxrwxr-x 1 ubuntu ubuntu 4096 Jan 12 15:21 ..
-????????? ? ?       ?          ?            ? data.txt
```

## Question 3

```shell
# (voir Makefile)
ubuntu:~$ make q3

ubuntu:~$ ls -al mydir/data.txt
-rw-rw-r-- 1 ubuntu ubuntu 37 Jan 12 16:14 essais/mydir/data.txt

ubuntu:~$ su toto
toto:~$ ls -al question3
-rwxrwxr-x 1 ubuntu ubuntu 17224 Jan 12 16:19 question3/a.out


toto:~$ question3/a.out mydir/data.txt
RUID: 1001, EUID: 1001, RGID: 1001, EGID: 1001
Cannot open file
: Permission denied

toto:~$ id
uid=1001(toto) gid=1001(toto) groups=1001(toto),1000(ubuntu)
```

Nous pouvons observer que les ids sont ceux de `toto` (car c'est `toto` qui a lancé le processus). Le programme n'arrive pas à lire le fichier car `toto` n'a pas le droit d'accès au dossier `mydir`, dans lequel est situé le fichier texte.

```shell
toto:~$ su ubuntu

# ajoute le flag set-user-id au programme
ubuntu:~$ chmod u+s question3/a.out

ubuntu:~$ ls -al question3/a.out
-rwsrwxr-x 1 florent florent 17224 Jan 12 16:19 a.out

ubuntu:~$ su toto
toto:~$ question3/a.out essais/mydir/data.txt
RUID: 1001, EUID: 1000, RGID: 1001, EGID: 1001
Bonjour, ceci est
le fichier data.txt
```

Après avoir ajouté le flag `set-user-id` au fichier exécutable, même si c'est `toto` qui lance le programme, le processus aura comme `EUID` celui du propriétaire de l'exécutable (`ubuntu`)

## Question 4

```shell
ubuntu:~$ chmod u+s question4/suid.py

ubuntu:~$ ls -al question4/suid.py

ubuntu:~$ su toto

#(voir le code de suid.py)
toto:~$ python3 question4/suid.py
EGID: 1001, EUID: 1001
```

On observe que l'EUID et le EGID sont ceux du groupe et de l'utilisateur `toto`. En effet, `toto` n'a pas "lancé" le script `suid.py`, il a lancé le programme `python3`, d'où le `EGID` et le `EUID` de `toto`.

## Question 5

`chfn` permet de modifier les informations d'un utilisateur. 

On visualise le contenu de `/etc/passwd` avec `cat`:
```shell
ubuntu:~$ cat /etc/passwd
...
toto:x:1001:1001:toto,1,,:/home/toto:/bin/bash
ubuntu:x:1000:1000:,,,:/home/ubuntu:/bin/bash
...
```

```shell
toto:~$ ls -al /usr/bin/chfn
-rwsr-xr-x 1 root root 85064 May 28  2020 /usr/bin/chfn
```

Les permissions de chfn sont les suivantes : 
1. propriétaire `root`:
    - lecture
    - ecriture
    - exécution avec set-user-id
2. groupe `root`:
    - lecture
    - exécuter
3. autre:
    - lecture
    - exécuter

```shell
toto:~$ chfn
Changing the user information for toto
Enter the new value, or press ENTER for the default
        Full Name: toto
        Room Number [1]: 20
        Work Phone []: 0612345678
        Home Phone []: 0611223344

toto:~$ cat /etc/passwd | grep toto
toto:x:1001:1001:toto,20,0612345678,0611223344:/home/toto:/bin/bash
```

Les informations de `toto` ont bien été modifiées !

## Question 6

Les mots de passe sont stockés dans `etc/shadow`. Voici les droits sur ce fichier:
```shell
ubuntu:~$ ls -al /etc/shadow
-rw-r----- 1 root shadow 1419 Jan 12 15:13 /etc/shadow
```

On observe que ce fichier appartient à `root`, que personne n'a le droit d'exécution sur le fichier, que seul l'utilisateur `root` peut apporter des modifications au fichier, et que seuls le groupe `shadow` et l'utilisateur `root` peuvent lire le contenu du fichier.

## Question 7

Voici la mise en place de la structure :
```shell
# Creation des groupes
ubuntu:~$ sudo addgroup admin
ubuntu:~$ sudo addgroup groupe_a
ubuntu:~$ sudo addgroup groupe_b
ubuntu:~$ sudo addgroup partage

# Creation des utilisateurs
ubuntu:~$ sudo adduser admin
ubuntu:~$ sudo adduser lambda_a
ubuntu:~$ sudo adduser lambda_b

# Ajout des groupes aux utilisateurs
ubuntu:~$ sudo usermod -a -G admin partage admin
ubuntu:~$ sudo usermod -a -G groupe_a partage lambda_a
ubuntu:~$ sudo usermod -a -G groupe_b partage lambda_b

# Creation des repertoires
ubuntu:~$ mkdir dir_a
ubuntu:~$ mkdir dir_b
ubuntu:~$ mkdir dir_c

# Changement de proprietaire des repertoires
ubuntu:~$ chown admin dir_a
ubuntu:~$ chown admin dir_b
ubuntu:~$ chown admin dir_c

# Changement du groupe des repertoires
ubuntu:~$ chgrp groupe_a dir_a
ubuntu:~$ chgrp groupe_b dir_b
ubuntu:~$ chgrp partage dir_c

ubuntu:~$ chmod 771 dir_a
ubuntu:~$ chmod g+s dir_a
ubuntu:~$ chmod +t dir_a

ubuntu:~$ chmod 771 dir_b
ubuntu:~$ chmod g+s dir_b
ubuntu:~$ chmod +t dir_b

ubuntu:~$ chmod 751 dir_c
ubuntu:~$ chmod +t dir_c

ubuntu:~$ ls -l
drwxrws--t 1 admin   groupe_a   4096 Jan 19 16:00 dir_a/
drwxrws--t 1 admin   groupe_b   4096 Jan 19 16:00 dir_b/
drwxr-x--t 1 admin   partage    4096 Jan 19 16:04 dir_c/
```

Nous avons créé un script par utilisateur : `admin.sh`, `lambda_a.sh`, `lambda_b.sh` (à utiliser avec l'utilisateur correspondant)
Voici l'exécution de `admin.sh` avec l'utilisateur admin:
```shell
# les commandes touch et rm ne produisent pas d'affichage dans le cas où il n'y a pas d'erreur
# les commandes ls du script ne produisant pas d'erreur n'affichent rien car il n'y a rien dans les dossiers
# il n'y a pas de message du type "Permission Denied" donc toutes les commandes se sont déroulées
# sans erreur
admin:~$ ./question7/admin.sh
```

Voici l'exécution de `lambda_a.sh` avec l'utilisateur `lambda_a`:
```shell
# les commandes touch et rm ne produisent pas d'affichage dans le cas où il n'y a pas d'erreur
# les commandes ls du script ne produisant pas d'erreur n'affichent rien car il n'y a rien dans les dossiers
lambda_a:~$ ./question7/lambda_a.sh
ls: cannot open directory 'dir_b': Permission denied
touch: cannot touch 'dir_b/test_a': Permission denied
touch: cannot touch 'dir_c/test_a': Permission denied
rm: cannot remove 'dir_b/test_a': No such file or directory
rm: cannot remove 'dir_c/test_a': No such file or directory
```

Voici l'exécution de `lambda_b.sh` avec l'utilisateur `lambda_b`:
```shell
# les commandes touch et rm ne produisent pas d'affichage dans le cas où il n'y a pas d'erreur
lambda_b:~$ ./question7/lambda_b.sh
ls: cannot open directory 'dir_a': Permission denied
touch: cannot touch 'dir_a/test_b': Permission denied
touch: cannot touch 'dir_c/test_b': Permission denied
rm: cannot remove 'dir_a/test_b': No such file or directory
rm: cannot remove 'dir_c/test_b': No such file or directory
```

## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








